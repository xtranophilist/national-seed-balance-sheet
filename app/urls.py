from django.conf.urls import patterns, include, url
from django.contrib import admin

from core import views as core_views

admin.autodiscover()

urlpatterns = patterns('',
                       # Examples:
                       url(r'^$', core_views.index, name='home'),
                       (r'^user/', include('users.urls')),
                       (r'^sheet/', include('sheet.urls')),
                       (r'^core/', include('core.urls')),
                       (r'^export/', include('export.urls')),
                       (r'^backup/', include('backup.urls')),

                       url(r'^admin/', include(admin.site.urls)),
)
