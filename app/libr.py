from core import arial10

ALPHABETS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
           'W', 'X', 'Y', 'Z']


def simple_dict(obj, name='name'):
    return {'id': obj.id, 'name': getattr(obj, name)}


class FitSheetWrapper(object):
    """Try to fit columns to max size of any entry.
    To use, wrap this around a worksheet returned from the
    workbook's add_sheet method, like follows:

        sheet = FitSheetWrapper(book.add_sheet(sheet_name))

    The worksheet interface remains the same: this is a drop-in wrapper
    for auto-sizing columns.
    """

    def __init__(self, sheet):
        self.sheet = sheet
        self.widths = dict()

    def write(self, r, c, label='', *args, **kwargs):
        self.sheet.write(r, c, label, *args, **kwargs)
        if type(label) not in [int, str, float]:
            width = 1800
        else:
            width = int(arial10.fitwidth(label))
        if width > self.widths.get(c, 0):
            self.widths[c] = width
            self.sheet.col(c).width = width
            if len(args) and args[0].alignment.rota is 90:
                self.sheet.col(c).width = 1500
                self.sheet.row(r).height = int(arial10.fitwidth(label) * 1.2)
            if len(args) and args[0].font.bold:
                self.sheet.col(c).width = int(arial10.fitwidth(label) * 1.2)
    def __getattr__(self, attr):
        return getattr(self.sheet, attr)