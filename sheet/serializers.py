from rest_framework.serializers import ModelSerializer
from sheet.models import Sheet, SheetColumn, SheetRow, SourceData, SourceColumn, SourceRow


class ColumnSerializer(ModelSerializer):
    class Meta:
        model = SheetColumn


class RowSerializer(ModelSerializer):
    columns = ColumnSerializer()

    class Meta:
        model = SheetRow


class SheetSerializer(ModelSerializer):
    rows = RowSerializer()

    class Meta:
        model = Sheet


class SourceColumnSerializer(ModelSerializer):
    class Meta:
        model = SourceColumn


class SourceRowSerializer(ModelSerializer):
    columns = SourceColumnSerializer()

    class Meta:
        model = SourceRow


class SourceDataSerializer(ModelSerializer):
    rows = SourceRowSerializer()

    class Meta:
        model = SourceData