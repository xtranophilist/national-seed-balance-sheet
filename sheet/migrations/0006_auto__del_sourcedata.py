# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'SourceData'
        db.delete_table(u'sheet_sourcedata')


    def backwards(self, orm):
        # Adding model 'SourceData'
        db.create_table(u'sheet_sourcedata', (
            ('source', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Source'])),
            ('variety', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Variety'])),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('quantity', self.gf('django.db.models.fields.FloatField')()),
        ))
        db.send_create_signal(u'sheet', ['SourceData'])


    models = {
        u'core.company': {
            'Meta': {'object_name': 'Company'},
            'address': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.District']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'phone_no': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'Company'", 'max_length': '7'})
        },
        u'core.crop': {
            'Meta': {'object_name': 'Crop'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_in_english': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'name_in_nepali': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'})
        },
        u'core.district': {
            'Meta': {'object_name': 'District'},
            'headquarter': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'zone': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Zone']"})
        },
        u'core.fiscalyear': {
            'Meta': {'object_name': 'FiscalYear'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        u'core.region': {
            'Meta': {'object_name': 'Region'},
            'headquarter': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'})
        },
        u'core.seedclass': {
            'Meta': {'object_name': 'SeedClass'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_in_english': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'name_in_nepali': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'})
        },
        u'core.source': {
            'Meta': {'object_name': 'Source'},
            'address': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.District']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'phone_no': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'})
        },
        u'core.variety': {
            'Meta': {'object_name': 'Variety'},
            'crop': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'varieties'", 'to': u"orm['core.Crop']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_in_english': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'name_in_nepali': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'})
        },
        u'core.zone': {
            'Meta': {'object_name': 'Zone'},
            'headquarter': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Region']"})
        },
        u'sheet.sheet': {
            'Meta': {'object_name': 'Sheet'},
            'crop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Crop']"}),
            'fiscal_year': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.FiscalYear']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Region']"}),
            'seed_class': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.SeedClass']"})
        },
        u'sheet.sheetcolumn': {
            'Meta': {'object_name': 'SheetColumn'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quantity': ('django.db.models.fields.FloatField', [], {}),
            'row': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'columns'", 'to': u"orm['sheet.SheetRow']"}),
            'variety': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Variety']"})
        },
        u'sheet.sheetrow': {
            'Meta': {'object_name': 'SheetRow'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sheet': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rows'", 'to': u"orm['sheet.Sheet']"}),
            'sources': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['core.Source']", 'symmetrical': 'False'})
        }
    }

    complete_apps = ['sheet']