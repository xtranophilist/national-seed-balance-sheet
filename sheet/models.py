from django.db import models
from core.models import Crop, SeedClass, Region, FiscalYear, Company, Source, Variety

# class Demand(models.Model):
#     fiscal_year = models.ForeignKey(FiscalYear)
#     variety = models.ForeignKey(Variety)
#     seed_class = models.ForeignKey(SeedClass)
#     company = models.ForeignKey(Company, null=True)
#
#
# class Fulfillment(models.Model):
#     demand_id = models.ForeignKey(Demand)
#     quantity = models.FloatField(null=True)
#     source = models.ForeignKey(Source, null=True)


class Sheet(models.Model):
    crop = models.ForeignKey(Crop)
    seed_class = models.ForeignKey(SeedClass)
    region = models.ForeignKey(Region)
    fiscal_year = models.ForeignKey(FiscalYear)
    order = models.CharField(max_length=254)

    def __str__(self):
        return str(self.crop) + ' - ' + str(self.seed_class) + ' - ' + str(self.region) + ' - ' + str(self.fiscal_year)


class SheetRow(models.Model):
    company = models.ForeignKey(Company, null=True)
    sources = models.ManyToManyField(Source)
    sheet = models.ForeignKey(Sheet, related_name='rows')

    def __str__(self):
        return str(self.company) + ' - ' + str(self.sheet)


class SheetColumn(models.Model):
    variety = models.ForeignKey(Variety)
    quantity = models.FloatField()
    row = models.ForeignKey(SheetRow, related_name='columns')

    def __str__(self):
        return str(self.variety) + ' - ' + str(self.row)



class SourceData(models.Model):
    source = models.ForeignKey(Source)
    crop = models.ForeignKey(Crop)
    seed_class = models.ForeignKey(SeedClass)
    fiscal_year = models.ForeignKey(FiscalYear)

    def __str__(self):
        return str(self.source) + ' - ' + str(self.crop) + ' - ' + str(self.seed_class) + ' - ' + str(self.fiscal_year)


class SourceDataRow(models.Model):
    variety = models.ForeignKey(Variety)
    quantity = models.FloatField()
    source_data = models.ForeignKey(SourceData, related_name='data_rows')

    def __str__(self):
        return str(self.variety) + ' - ' + str(self.source_data)


class SourceRow(models.Model):
    company = models.ForeignKey(Company, null=True)
    source_data = models.ForeignKey(SourceData, related_name='rows')


class SourceColumn(models.Model):
    variety = models.ForeignKey(Variety)
    quantity = models.FloatField()
    row = models.ForeignKey(SourceRow, related_name='columns')