from django.conf.urls import patterns, url
from sheet import views


urlpatterns = patterns('',
                       url(
                           r'^source_data/(?P<fiscal_year_id>[0-9]+)/(?P<source_id>[0-9]+)/(?P<crop_id>[0-9]+)/(?P<seed_class_id>[0-9]+)/$',
                           views.view_source, name='view_source'),
                       url(
                           r'^(?P<fiscal_year_id>[0-9]+)/(?P<region_id>[0-9]+)/(?P<crop_id>[0-9]+)/(?P<seed_class_id>[0-9]+)/$',
                           views.view_sheet, name='view_sheet'),
                       url(r'^save/$', views.save_sheet, name='save_sheet'),
                       url(r'^source_data/save/$', views.save_source_data, name='save_source_data'),
                       url(
                           r'^source/(?P<fiscal_year_id>[0-9]+)/(?P<source_id>[0-9]+)/(?P<crop_id>[0-9]+)/(?P<seed_class_id>[0-9]+)/$',
                           views.view_source_sheet, name='view_source_sheet'),
                       url(r'^source/save/$', views.save_source_sheet, name='save_source_sheet'),

)
