import json
from django.contrib.auth.decorators import login_required

from django.http import HttpResponse

from django.shortcuts import render, get_object_or_404
from app.libr import simple_dict

from core.models import FiscalYear, Region, Crop, SeedClass, Company, Source
from sheet.models import Sheet, SheetRow, SheetColumn, SourceDataRow, SourceData, SourceRow, SourceColumn
from sheet.serializers import SheetSerializer, SourceDataSerializer


@login_required
def view_sheet(request, fiscal_year_id, region_id, crop_id, seed_class_id):
    #id on urls to their objects
    fiscal_year = get_object_or_404(FiscalYear, id=fiscal_year_id)
    region = get_object_or_404(Region, id=region_id)
    crop = get_object_or_404(Crop, id=crop_id)
    seed_class = get_object_or_404(SeedClass, id=seed_class_id)
    try:
        sheet = Sheet.objects.get(fiscal_year=fiscal_year, region=region, crop=crop, seed_class=seed_class)
        created = False
    except Sheet.DoesNotExist:
        sheet = Sheet(fiscal_year=fiscal_year, region=region, crop=crop, seed_class=seed_class)
        created = True
    sheet_data = SheetSerializer(sheet).data

    varieties = []
    for variety in crop.varieties.all():
        varieties.append({'id': variety.id, 'name': variety.name_in_english})

    context = {
        'sheet': sheet_data,
        'varieties': varieties,
        'created': created,
        'data': sheet,
    }
    return render(request, 'sheet.html', context)

# @login_required
# def view_sheet(request, fiscal_year_id, region_id, crop_id, seed_class_id):
#     #id on urls to their objects
#     fiscal_year = get_object_or_404(FiscalYear, id=fiscal_year_id)
#     region = get_object_or_404(Region, id=region_id)
#     crop = get_object_or_404(Crop, id=crop_id)
#     seed_class = get_object_or_404(SeedClass, id=seed_class_id)
#     client = MongoClient()
#     db = client.seed
#     sheets = db.sheets
#     sheet = sheets.find_one(
#         {'fiscal_year.id': fiscal_year_id, 'region.id': region_id, 'crop.id': crop_id, 'seed_class.id': seed_class_id})
#     if not sheet:
#         sheet = {
#             'fiscal_year': {'id': fiscal_year_id, 'name': fiscal_year.name},
#             'region': {'id': region_id, 'name': region.name},
#             'crop': {'id': crop_id, 'name': crop.name_in_english},
#             'seed_class': {'id': seed_class_id, 'name': seed_class.name_in_english},
#         }
#         #try:
#     #    sheet = Sheet.objects.get(fiscal_year=fiscal_year, region=region, crop=crop, seed_class=seed_class)
#     #    created = False
#     #except Sheet.DoesNotExist:
#     #    sheet = Sheet(fiscal_year=fiscal_year, region=region, crop=crop, seed_class=seed_class)
#     #    created = True
#     varieties = []
#     for variety in crop.varieties.all():
#         varieties.append({'id': variety.id, 'name': variety.name_in_english})
#     companies = []
#     for company in Company.objects.filter(district__zone__region=region):
#         companies.append({'id': company.id, 'name': company.name, 'district': company.district.id})
#     sources = []
#     for source in Source.objects.filter(district__zone__region=region):
#         sources.append({'id': source.id, 'name': source.name, 'district': source.district.id})
#     context = {
#         'sheet': sheet,
#         #'new': created,
#         'varieties': varieties,
#         'companies': companies,
#         'sources': sources,
#     }
#     return render(request, 'sheet.html', context)

@login_required
def save_sheet(request):
    dct = {'invalid_attributes': {}, 'saved': {}}
    body = json.loads(request.body)
    if body['id']:
        sheet = Sheet.objects.get(id=body['id'])
        sheet.rows.all().delete()
    else:
        sheet = Sheet(crop_id=body['crop'], fiscal_year_id=body['fiscal_year'], region_id=body['region'],
                      seed_class_id=body['seed_class'])

    order = []
    for index, variety in enumerate(body['varieties']):
        order.append(variety['id'])
    sheet.order = ','.join([str(i) for i in order])
    sheet.save()

    for row in body['rows']:
        company_id = row['company_id']
        if company_id == '':
            company_id = None
        r = SheetRow(sheet=sheet, company_id=company_id)
        r.save()
        for src in row.get('sources'):
            source = Source.objects.get(id=src)
            r.sources.add(source)
        for k, value in enumerate(row['values']):
            if value != '' and value is not None:
                column = SheetColumn(variety_id=order[k], quantity=float(value), row=r)
                column.save()

    dct['id'] = sheet.id
    return HttpResponse(json.dumps(dct), mimetype="application/json")


def view_source(request, fiscal_year_id, source_id, crop_id, seed_class_id):
    fiscal_year = get_object_or_404(FiscalYear, id=fiscal_year_id)
    source = get_object_or_404(Source, id=source_id)
    crop = get_object_or_404(Crop, id=crop_id)
    seed_class = get_object_or_404(SeedClass, id=seed_class_id)
    try:
        source_data = SourceData.objects.get(fiscal_year=fiscal_year, source=source, crop=crop, seed_class=seed_class)
    except SourceData.DoesNotExist:
        source_data = SourceData(fiscal_year=fiscal_year, source=source, crop=crop, seed_class=seed_class)
        source_data.save()
    varieties = []
    for variety in crop.varieties.all():
        try:
            source_row = SourceDataRow.objects.get(variety=variety, source_data=source_data)
            quantity = source_row.quantity
        except SourceDataRow.DoesNotExist:
            quantity = None
        varieties.append({'id': variety.id, 'name': variety.name_in_english, 'quantity': quantity})
    context = {
        'data': {
            'fiscal_year': simple_dict(fiscal_year),
            'seed_class': simple_dict(seed_class, 'name_in_english'),
            'varieties': varieties,
            'source': simple_dict(source),
            'crop': simple_dict(crop, 'name_in_english'),
            'id': source_data.id,
        }
    }
    return render(request, 'view_source.html', context)


def save_source_data(request):
    dct = {'invalid_attributes': {}, 'saved': [], 'deleted': []}
    body = json.loads(request.body)
    source_data = SourceData.objects.get(id=body.get('id'))
    varieties = body.get('varieties')
    for variety in varieties:
        if variety.get('quantity'):
            try:
                source_data_row = SourceDataRow.objects.get(variety_id=variety.get('id'), source_data_id=source_data.id)
            except SourceDataRow.DoesNotExist:
                source_data_row = SourceDataRow(variety_id=variety.get('id'), source_data_id=source_data.id)
            source_data_row.quantity = variety.get('quantity')
            source_data_row.save()
            dct['saved'].append(source_data_row.id)
        else:
            try:
                source_data_row = SourceDataRow.objects.get(variety_id=variety.get('id'), source_data_id=source_data.id)
                dct['deleted'].append(source_data_row.id)
                source_data_row.delete()
            except SourceDataRow.DoesNotExist:
                pass
    return HttpResponse(json.dumps(dct), mimetype="application/json")


@login_required
def view_source_sheet(request, fiscal_year_id, source_id, crop_id, seed_class_id):
    #id on urls to their objects
    fiscal_year = get_object_or_404(FiscalYear, id=fiscal_year_id)
    source = get_object_or_404(Source, id=source_id)
    crop = get_object_or_404(Crop, id=crop_id)
    seed_class = get_object_or_404(SeedClass, id=seed_class_id)
    try:
        sheet = SourceData.objects.get(fiscal_year=fiscal_year, source=source, crop=crop, seed_class=seed_class)
        created = False
    except SourceData.DoesNotExist:
        sheet = SourceData(fiscal_year=fiscal_year, source=source, crop=crop, seed_class=seed_class)
        created = True
    sheet_data = SourceDataSerializer(sheet).data

    varieties = []
    for variety in crop.varieties.all():
        try:
            source_row = SourceDataRow.objects.get(variety=variety, source_data=sheet)
            quantity = source_row.quantity
        except SourceDataRow.DoesNotExist:
            quantity = None
        varieties.append({'id': variety.id, 'name': variety.name_in_english, 'quantity': quantity})

    context = {
        'sheet': sheet_data,
        'varieties': varieties,
        'created': created,
        'data': sheet,
    }
    return render(request, 'source_sheet.html', context)


@login_required
def save_source_sheet(request):
    dct = {'invalid_attributes': {}, 'saved': [], 'deleted': []}
    body = json.loads(request.body)
    if body['id']:
        sheet = SourceData.objects.get(id=body['id'])
        sheet.rows.all().delete()
    else:
        sheet = SourceData(crop_id=body['crop'], fiscal_year_id=body['fiscal_year'], source_id=body['source'],
                           seed_class_id=body['seed_class'])
    sheet.save()

    varieties = body.get('varieties')
    for variety in varieties:
        if variety.get('quantity'):
            try:
                source_data_row = SourceDataRow.objects.get(variety_id=variety.get('id'), source_data_id=sheet.id)
            except SourceDataRow.DoesNotExist:
                source_data_row = SourceDataRow(variety_id=variety.get('id'), source_data_id=sheet.id)
            source_data_row.quantity = variety.get('quantity')
            source_data_row.save()
            dct['saved'].append(source_data_row.id)
        else:
            try:
                source_data_row = SourceDataRow.objects.get(variety_id=variety.get('id'), source_data_id=sheet.id)
                dct['deleted'].append(source_data_row.id)
                source_data_row.delete()
            except SourceDataRow.DoesNotExist:
                pass

    for row in body['rows']:
        company_id = row['company_id']
        if company_id == '':
            company_id = None
        r = SourceRow(source_data=sheet, company_id=company_id)
        r.save()
        for k, value in enumerate(row['values']):
            if value != '' and value is not None:
                column = SourceColumn(variety_id=body['varieties'][k]['id'], quantity=float(value), row=r)
                column.save()

    dct['id'] = sheet.id
    return HttpResponse(json.dumps(dct), mimetype="application/json")