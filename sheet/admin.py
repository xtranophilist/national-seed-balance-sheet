from django.contrib import admin
from sheet.models import Sheet, SourceData, SourceDataRow, SheetRow, SheetColumn

admin.site.register(Sheet)
admin.site.register(SheetRow)
admin.site.register(SheetColumn)
admin.site.register(SourceData)
admin.site.register(SourceDataRow)
