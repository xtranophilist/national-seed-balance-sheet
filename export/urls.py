from django.conf.urls import patterns, url
from export import views


urlpatterns = patterns('',
                       url(r'^$', views.index, name='export_index'),
                       url(r'^balance_by_class$', views.balance_by_class, name='balance_by_class'),
                       url(r'^balance_by_region$', views.balance_by_region, name='balance_by_region'),
                       url(r'^source_data$', views.source_data, name='export_source_data'),
                       url(r'^regional$', views.regional, name='export_regional'),
)
