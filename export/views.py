from django.http import HttpResponse
from django.shortcuts import render
from xlwt import Workbook, easyxf, Formula
from app.libr import FitSheetWrapper, ALPHABETS
from core.models import FiscalYear, Region, Crop, Source, SeedClass
from sheet.models import SheetRow, SourceRow, SourceData, SheetColumn
from django.db.models import Sum


def index(request):
    context = {
        'fiscal_years': FiscalYear.objects.all(),
        'regions': Region.objects.all(),
        'crops': Crop.objects.all(),
        'sources': Source.objects.all(),
        'seed_classes': SeedClass.objects.all()
    }
    return render(request, 'export_index.html', context)


def balance_by_class(request):
    fiscal_year = FiscalYear.objects.get(id=request.GET.get('fiscal_year'))
    seed_class = SeedClass.objects.get(id=request.GET.get('seed_class'))
    response = HttpResponse(mimetype="application/ms-excel")
    response['Content-Disposition'] = 'attachment; filename=' + str(seed_class) + '-' + str(fiscal_year) + '.xls'
    sorted_by_crop = {}
    book = Workbook()

    all_rows = SheetRow.objects.filter(sheet__fiscal_year_id=request.GET.get('fiscal_year'),
                                       sheet__seed_class_id=request.GET.get('seed_class')).select_related('sheet')
    for a_row in all_rows:
        try:
            sorted_by_crop[a_row.sheet.crop].append(a_row)
        except KeyError:
            sorted_by_crop[a_row.sheet.crop] = [a_row]
    for crop in sorted_by_crop:
        # sheet = (book.add_sheet(str(crop)))
        sheet = FitSheetWrapper((book.add_sheet(str(crop))))
        # the header
        # bold_bordered = easyxf("font: bold on; borders: top thin, right thin, bottom thin, left thin;")
        # bordered = easyxf("borders: top thin, right thin, bottom thin, left thin;")
        bold_bordered = easyxf("font: bold on")
        bordered = easyxf("")
        rotated = easyxf("font: bold on; align: rotation 90")
        sheet.write(0, 0, 'S.N.', bold_bordered)
        sheet.write(0, 1, 'DADO/Company', bold_bordered)
        for key, variety in enumerate(crop.varieties.all()):
            sheet.write(0, 2 + key, str(variety), rotated)
        number_of_varieties = key + 1
        sheet.write(0, 3 + key, 'Total', rotated)
        sheet.write(0, 4 + key, 'Source Center', bold_bordered)
        for row_key, row in enumerate(sorted_by_crop[crop]):
            row_count = row_key + 1
            sheet.write(row_count, 0, 1 + row_key, bordered)
            sheet.write(row_count, 1, str(row.company), bordered)
            for column in row.columns.all():
                for variety_key, variety in enumerate(crop.varieties.all()):
                    if variety == column.variety:
                        sheet.write(row_count, 2 + variety_key, column.quantity, bordered)
            sheet.write(row_count, 3 + variety_key, Formula(
                "SUM(C" + str(row_count + 1) + ":" + ALPHABETS[number_of_varieties + 1] + str(
                    row_count + 1) + ")"), bordered)
            sheet.write(row_count, 4 + variety_key, ', '.join([str(x) for x in row.sources.all()]), bordered)
        sheet.write(row_count + 1, 1, 'Total', bold_bordered)
        for key, variety in enumerate(crop.varieties.all()):
            sheet.write(row_count + 1, 2 + key,
                        Formula("SUM(" + ALPHABETS[key + 2] + "2:" + ALPHABETS[key + 2] + str(row_key + 2) + ")"))
        sheet.write(row_count + 1, number_of_varieties + 2,
                    Formula("SUM(C" + str(row_count + 2) + ":" + ALPHABETS[number_of_varieties + 1] + str(
                        row_count + 2) + ")"), bordered)
    book.save(response)
    return response


def balance_by_region(request):
    fiscal_year = FiscalYear.objects.get(id=request.GET.get('fiscal_year'))
    region = Region.objects.get(id=request.GET.get('region'))
    response = HttpResponse(mimetype="application/ms-excel")
    response['Content-Disposition'] = 'attachment; filename=' + str(region) + '-' + str(fiscal_year) + '.xls'
    sorted_by_crop = {}
    book = Workbook()
    all_rows = SheetRow.objects.filter(sheet__fiscal_year_id=request.GET.get('fiscal_year'),
                                       sheet__region_id=request.GET.get('region')).select_related('sheet')
    for a_row in all_rows:
        try:
            sorted_by_crop[(a_row.sheet.crop, a_row.sheet.seed_class)].append(a_row)
        except KeyError:
            sorted_by_crop[(a_row.sheet.crop, a_row.sheet.seed_class)] = [a_row]
    for tup in sorted_by_crop:
        crop = tup[0]
        seed_class = tup[1]
        # sheet = (book.add_sheet(str(crop)))
        sheet = FitSheetWrapper((book.add_sheet(str(crop) + ' - ' + str(seed_class))))
        # the header
        # bold_bordered = easyxf("font: bold on; borders: top thin, right thin, bottom thin, left thin;")
        # bordered = easyxf("borders: top thin, right thin, bottom thin, left thin;")
        bold_bordered = easyxf("font: bold on")
        bordered = easyxf("")
        rotated = easyxf("font: bold on; align: rotation 90")
        sheet.write(0, 0, 'S.N.', bold_bordered)
        sheet.write(0, 1, 'DADO/Company', bold_bordered)
        for key, variety in enumerate(crop.varieties.all()):
            sheet.write(0, 2 + key, str(variety), rotated)
        number_of_varieties = key + 1
        sheet.write(0, 3 + key, 'Total', rotated)
        sheet.write(0, 4 + key, 'Source Center', bold_bordered)
        for row_key, row in enumerate(sorted_by_crop[tup]):
            row_count = row_key + 1
            sheet.write(row_count, 0, 1 + row_key, bordered)
            sheet.write(row_count, 1, str(row.company), bordered)
            for column in row.columns.all():
                for variety_key, variety in enumerate(crop.varieties.all()):
                    if variety == column.variety:
                        sheet.write(row_count, 2 + variety_key, column.quantity, bordered)
            sheet.write(row_count, 3 + variety_key, Formula(
                "SUM(C" + str(row_count + 1) + ":" + ALPHABETS[number_of_varieties + 1] + str(
                    row_count + 1) + ")"), bordered)
            sheet.write(row_count, 4 + variety_key, ', '.join([str(x) for x in row.sources.all()]), bordered)
        sheet.write(row_count + 1, 1, 'Total', bold_bordered)
        for key, variety in enumerate(crop.varieties.all()):
            sheet.write(row_count + 1, 2 + key,
                        Formula("SUM(" + ALPHABETS[key + 2] + "2:" + ALPHABETS[key + 2] + str(row_key + 2) + ")"))
        sheet.write(row_count + 1, number_of_varieties + 2,
                    Formula("SUM(C" + str(row_count + 2) + ":" + ALPHABETS[number_of_varieties + 1] + str(
                        row_count + 2) + ")"), bordered)
    book.save(response)
    return response


def source_data(request):
    fiscal_year = FiscalYear.objects.get(id=request.GET.get('fiscal_year'))
    crop = Crop.objects.get(id=request.GET.get('crop'))
    response = HttpResponse(mimetype="application/ms-excel")
    response['Content-Disposition'] = 'attachment; filename=' + str(crop) + '-' + str(fiscal_year) + '.xls'
    sorted_by_source = {}
    book = Workbook()
    all_rows = SourceRow.objects.filter(source_data__fiscal_year_id=request.GET.get('fiscal_year'),
                                        source_data__crop_id=request.GET.get('crop')).select_related('source_data')
    for a_row in all_rows:
        try:
            sorted_by_source[(a_row.source_data.source, a_row.source_data.seed_class)].append(a_row)
        except KeyError:
            sorted_by_source[(a_row.source_data.source, a_row.source_data.seed_class)] = [a_row]
    for tup in sorted_by_source:
        source = tup[0]
        seed_class = tup[1]
        # limit worksheet name to 30 chars
        worksheet_name = str(source)[0:27 - len(str(seed_class))] + ' - ' + str(seed_class)
        sheet = FitSheetWrapper((book.add_sheet(worksheet_name)))
        # the header
        # bold_bordered = easyxf("font: bold on; borders: top thin, right thin, bottom thin, left thin;")
        # bordered = easyxf("borders: top thin, right thin, bottom thin, left thin;")
        bold_bordered = easyxf("font: bold on")
        bordered = easyxf("")
        rotated = easyxf("font: bold on; align: rotation 90")
        sheet.write(0, 0, 'S.N.', bold_bordered)
        sheet.write(0, 1, 'DADO/Company', bold_bordered)
        for key, variety in enumerate(crop.varieties.all()):
            sheet.write(0, 2 + key, str(variety), rotated)
        number_of_varieties = key + 1
        sheet.write(0, 3 + key, 'Total', rotated)
        sheet.write(1, 1, 'Seed Production (kg)', bold_bordered)
        data = SourceData.objects.get(source=source, seed_class=seed_class, crop=crop, fiscal_year=fiscal_year)
        for datum in data.data_rows.all():
            for variety_key, variety in enumerate(crop.varieties.all()):
                if variety == datum.variety:
                    sheet.write(1, 2 + variety_key, datum.quantity, bordered)
        sheet.write(1, 3 + variety_key, Formula("SUM(C2:" + ALPHABETS[number_of_varieties + 1] + "2)"), bordered)
        for row_key, row in enumerate(sorted_by_source[tup]):
            row_count = row_key + 2
            sheet.write(row_count, 0, 1 + row_key, bordered)
            sheet.write(row_count, 1, str(row.company), bordered)
            for column in row.columns.all():
                for variety_key, variety in enumerate(crop.varieties.all()):
                    if variety == column.variety:
                        sheet.write(row_count, 2 + variety_key, column.quantity, bordered)
            sheet.write(row_count, 3 + variety_key, Formula(
                "SUM(C" + str(row_count + 1) + ":" + ALPHABETS[number_of_varieties + 1] + str(
                    row_count + 1) + ")"), bordered)
        sheet.write(row_count + 1, 1, 'Total', bold_bordered)
        for key, variety in enumerate(crop.varieties.all()):
            sheet.write(row_count + 1, 2 + key,
                        Formula("SUM(" + ALPHABETS[key + 2] + "3:" + ALPHABETS[key + 2] + str(row_key + 3) + ")"))
        sheet.write(row_count + 1, number_of_varieties + 2,
                    Formula("SUM(C" + str(row_count + 2) + ":" + ALPHABETS[number_of_varieties + 1] + str(
                        row_count + 2) + ")"), bordered)
        sheet.write(row_count + 2, 1, 'Surplus', bold_bordered)
        for key, variety in enumerate(crop.varieties.all()):
            sheet.write(row_count + 2, 2 + key,
                        Formula(ALPHABETS[key + 2] + "2-" + ALPHABETS[key + 2] + str(row_count + 2)))
        sheet.write(row_count + 2, number_of_varieties + 2,
                    Formula("SUM(C" + str(row_count + 3) + ":" + ALPHABETS[number_of_varieties + 1] + str(
                        row_count + 3) + ")"), bordered)
    book.save(response)
    return response


def regional(request):
    fiscal_year = FiscalYear.objects.get(id=request.GET.get('fiscal_year'))
    response = HttpResponse(mimetype="application/ms-excel")
    response['Content-Disposition'] = 'attachment; filename=Regional-' + str(fiscal_year) + '.xls'
    sorted_by_crop = {}
    book = Workbook()
    all_rows = SheetRow.objects.filter(sheet__fiscal_year_id=request.GET.get('fiscal_year')).select_related('sheet')
    for a_row in all_rows:
        try:
            sorted_by_crop[(a_row.sheet.crop, a_row.sheet.seed_class)].append(a_row)
        except KeyError:
            sorted_by_crop[(a_row.sheet.crop, a_row.sheet.seed_class)] = [a_row]
    for tup in sorted_by_crop:
        crop = tup[0]
        seed_class = tup[1]
        sheet = FitSheetWrapper((book.add_sheet(str(crop) + ' - ' + str(seed_class))))
        bold_bordered = easyxf("font: bold on; borders: top thin, right thin, bottom thin, left thin;")
        bordered = easyxf("borders: top thin, right thin, bottom thin, left thin;")
        sheet.write(0, 0, 'S.N.', bold_bordered)
        sheet.write(0, 1, 'Variety', bold_bordered)
        regions = Region.objects.all()

        for region_key, region in enumerate(regions):
            sheet.write(0, 2 + region_key, str(region), bold_bordered)
        sheet.write(0, 3 + region_key, 'Total', bold_bordered)

        for variety_key, variety in enumerate(crop.varieties.all()):
            sheet.write(variety_key + 1, 0, variety_key + 1, bordered)
            sheet.write(variety_key + 1, 1, str(variety), bordered)
            for region_key, region in enumerate(regions):
                the_sum = SheetColumn.objects.filter(row__sheet__region=region, variety=variety,
                                                     row__sheet__seed_class=seed_class).aggregate(
                    total=Sum('quantity'))['total']
                sheet.write(variety_key + 1, 2 + region_key, the_sum, bordered)
            sheet.write(variety_key + 1, region_key + 3, Formula(
                "SUM(C" + str(variety_key + 2) + ":" + ALPHABETS[region_key + 2] + str(
                    variety_key + 2) + ")"), bold_bordered)
        sheet.write(variety_key + 2, 1, 'Total', bold_bordered)

        for region_key, region in enumerate(regions):
            sheet.write(variety_key + 2, 2 + region_key, Formula(
                "SUM(" + ALPHABETS[region_key + 2] + "2:" + ALPHABETS[region_key + 2] + str(variety_key + 2) + ")"
            ), bold_bordered)
        sheet.write(variety_key + 2, 2 + region_key + 1, Formula(
            "SUM(" + ALPHABETS[region_key + 3] + "2:" + ALPHABETS[region_key + 3] + str(variety_key + 2) + ")"
        ), bold_bordered)

    book.save(response)
    return response