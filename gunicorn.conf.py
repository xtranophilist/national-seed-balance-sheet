import multiprocessing

bind = "0.0.0.0:8000"
workers = 3
errorlog = '../log/gunicorn.log'
#accesslog = 'gunicorn-access.log'
#loglevel = 'debug'
proc_name = 'gunicorn_acubor'
