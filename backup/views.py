from django.http import HttpResponse
from datetime import date


def index(request):
    from app.settings import DATABASES

    default = DATABASES['default']
    if default['ENGINE'] == 'django.db.backends.sqlite3':
        db_name = default['NAME']
        from django.core.files import File

        dbfile = File(open(db_name, "rb"))
        response = HttpResponse(dbfile, mimetype='application/x-sqlite3')
        response['Content-Disposition'] = 'attachment; filename=seed_bs-' + str(date.today())+'.bk'
        response['Content-Length'] = dbfile.size
        return response
