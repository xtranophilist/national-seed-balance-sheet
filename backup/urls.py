from django.conf.urls import patterns, url
from backup import views


urlpatterns = patterns('',
                       url(r'^$', views.index, name='backup_index'),
)
