import json
from django.http import HttpResponse
from django.shortcuts import render
from django.contrib.auth.views import login
from core.models import FiscalYear, Region, Crop, SeedClass, Company, Source
from core.serializers import CompanySerializer, SourceSerializer, SourceVarietySerializer


def index(request):
    if request.user.is_authenticated():
        context = {
            'fiscal_years': FiscalYear.objects.all(),
            'regions': Region.objects.all(),
            'crops': Crop.objects.all(),
            'sources': Source.objects.all(),
            'seed_classes': SeedClass.objects.all(),
        }
        return render(request, 'dashboard_index.html', context)
    return login(request)


def companies_as_json(request, region_id=None):
    if region_id:
        objects = Company.objects.filter(district__zone__region_id=region_id)
    else:
        objects = Company.objects.all()
    objects_data = CompanySerializer(objects, many=True).data
    return HttpResponse(json.dumps(objects_data), mimetype="application/json")


def sources_as_json(request):
    objects = Source.objects.all()
    objects_data = SourceSerializer(objects, many=True).data
    return HttpResponse(json.dumps(objects_data), mimetype="application/json")

def sources_with_varieties_as_json(request, fiscal_year_id, crop_id, seed_class_id):
    objects = Source.objects.all()
    objects_data = SourceVarietySerializer(objects, fiscal_year_id=fiscal_year_id, crop_id=crop_id,seed_class_id=seed_class_id, many=True).data
    return HttpResponse(json.dumps(objects_data), mimetype="application/json")