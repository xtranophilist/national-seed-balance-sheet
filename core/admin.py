from django.contrib import admin
from core.models import District, Region, Zone
from core.models import SeedClass, Variety, Crop, FiscalYear, Company, Source

#admin.site.register(District)
#admin.site.register(Zone)
admin.site.register(Region)

admin.site.register(SeedClass)
admin.site.register(Variety)
admin.site.register(Crop)
admin.site.register(FiscalYear)
admin.site.register(Company)
admin.site.register(Source)
