# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'FiscalYear'
        db.create_table(u'core_fiscalyear', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=15)),
        ))
        db.send_create_signal(u'core', ['FiscalYear'])


    def backwards(self, orm):
        # Deleting model 'FiscalYear'
        db.delete_table(u'core_fiscalyear')


    models = {
        u'core.crop': {
            'Meta': {'object_name': 'Crop'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_in_english': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'name_in_nepali': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'})
        },
        u'core.district': {
            'Meta': {'object_name': 'District'},
            'headquarter': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'zone': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Zone']"})
        },
        u'core.fiscalyear': {
            'Meta': {'object_name': 'FiscalYear'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        u'core.region': {
            'Meta': {'object_name': 'Region'},
            'headquarter': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'})
        },
        u'core.seedclass': {
            'Meta': {'object_name': 'SeedClass'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_in_english': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'name_in_nepali': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'})
        },
        u'core.variety': {
            'Meta': {'object_name': 'Variety'},
            'crop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Crop']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_in_english': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'name_in_nepali': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'})
        },
        u'core.zone': {
            'Meta': {'object_name': 'Zone'},
            'headquarter': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Region']"})
        }
    }

    complete_apps = ['core']