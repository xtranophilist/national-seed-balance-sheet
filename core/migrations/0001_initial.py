# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Crop'
        db.create_table(u'core_crop', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name_in_english', self.gf('django.db.models.fields.CharField')(max_length=254)),
            ('name_in_nepali', self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True)),
        ))
        db.send_create_signal(u'core', ['Crop'])

        # Adding model 'Variety'
        db.create_table(u'core_variety', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name_in_english', self.gf('django.db.models.fields.CharField')(max_length=254)),
            ('name_in_nepali', self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True)),
            ('crop', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Crop'])),
        ))
        db.send_create_signal(u'core', ['Variety'])

        # Adding model 'SeedClass'
        db.create_table(u'core_seedclass', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name_in_english', self.gf('django.db.models.fields.CharField')(max_length=254)),
            ('name_in_nepali', self.gf('django.db.models.fields.CharField')(max_length=254, null=True, blank=True)),
        ))
        db.send_create_signal(u'core', ['SeedClass'])

        # Adding model 'Region'
        db.create_table(u'core_region', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=254)),
            ('headquarter', self.gf('django.db.models.fields.CharField')(max_length=254)),
        ))
        db.send_create_signal(u'core', ['Region'])

        # Adding model 'Zone'
        db.create_table(u'core_zone', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Region'])),
            ('headquarter', self.gf('django.db.models.fields.CharField')(max_length=254)),
        ))
        db.send_create_signal(u'core', ['Zone'])

        # Adding model 'District'
        db.create_table(u'core_district', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=25)),
            ('zone', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Zone'])),
            ('headquarter', self.gf('django.db.models.fields.CharField')(max_length=254)),
            ('latitude', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('longitude', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'core', ['District'])


    def backwards(self, orm):
        # Deleting model 'Crop'
        db.delete_table(u'core_crop')

        # Deleting model 'Variety'
        db.delete_table(u'core_variety')

        # Deleting model 'SeedClass'
        db.delete_table(u'core_seedclass')

        # Deleting model 'Region'
        db.delete_table(u'core_region')

        # Deleting model 'Zone'
        db.delete_table(u'core_zone')

        # Deleting model 'District'
        db.delete_table(u'core_district')


    models = {
        u'core.crop': {
            'Meta': {'object_name': 'Crop'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_in_english': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'name_in_nepali': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'})
        },
        u'core.district': {
            'Meta': {'object_name': 'District'},
            'headquarter': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'zone': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Zone']"})
        },
        u'core.region': {
            'Meta': {'object_name': 'Region'},
            'headquarter': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'})
        },
        u'core.seedclass': {
            'Meta': {'object_name': 'SeedClass'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_in_english': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'name_in_nepali': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'})
        },
        u'core.variety': {
            'Meta': {'object_name': 'Variety'},
            'crop': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Crop']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_in_english': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'name_in_nepali': ('django.db.models.fields.CharField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'})
        },
        u'core.zone': {
            'Meta': {'object_name': 'Zone'},
            'headquarter': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Region']"})
        }
    }

    complete_apps = ['core']