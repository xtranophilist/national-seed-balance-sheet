from rest_framework import serializers
from core.models import Company, Source
from sheet.models import SourceData


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company


class SourceSerializer(serializers.ModelSerializer):
    latitude = serializers.Field(source='district.latitude')
    longitude = serializers.Field(source='district.latitude')

    class Meta:
        model = Source
        exclude = ['phone_no', 'address']


class SourceVarietySerializer(serializers.ModelSerializer):
    latitude = serializers.Field(source='district.latitude')
    longitude = serializers.Field(source='district.latitude')
    data = serializers.SerializerMethodField('get_data')

    def __init__(self, *args, **kwargs):
        self.fiscal_year_id = kwargs.pop('fiscal_year_id')
        self.crop_id = kwargs.pop('crop_id')
        self.seed_class_id = kwargs.pop('seed_class_id')
        super(SourceVarietySerializer, self).__init__(*args, **kwargs)

    def get_data(self, obj):
        data = []
        sources_data = SourceData.objects.filter(fiscal_year_id=self.fiscal_year_id, crop_id=self.crop_id, seed_class_id=self.seed_class_id, source=obj)
        for source_data in sources_data:
            rows = source_data.data_rows.all()
            for row in rows:
                dt = {'variety_name': row.variety.name_in_english, 'variety_id': row.variety.id, 'quantity': row.quantity}
                data.append(dt)
        return data

    class Meta:
        model = Source
        exclude = ['phone_no', 'address']