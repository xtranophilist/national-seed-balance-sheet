from django.db import models


class FiscalYear(models.Model):
    name = models.CharField(max_length=15)

    def __str__(self):
        return self.name


class Crop(models.Model):
    name_in_english = models.CharField(max_length=254)
    name_in_nepali = models.CharField(max_length=254, blank=True, null=True)
    type = models.CharField(max_length=254, blank=True, null=True)

    def __str__(self):
        return self.name_in_english


class Variety(models.Model):
    name_in_english = models.CharField(max_length=254)
    name_in_nepali = models.CharField(max_length=254, blank=True, null=True)
    crop = models.ForeignKey(Crop, related_name='varieties')

    def __str__(self):
        return self.name_in_english

    class Meta:
        verbose_name_plural = u'Varieties'


class SeedClass(models.Model):
    name_in_english = models.CharField(max_length=254)
    name_in_nepali = models.CharField(max_length=254, blank=True, null=True)

    def __str__(self):
        return self.name_in_english

    class Meta:
        verbose_name = u'Class'
        verbose_name_plural = u'Classes'


class Region(models.Model):
    name = models.CharField(max_length=254)
    headquarter = models.CharField(max_length=254)

    def __str__(self):
        return self.name


class Zone(models.Model):
    name = models.CharField(max_length=15)
    region = models.ForeignKey(Region)
    headquarter = models.CharField(max_length=254)

    def __str__(self):
        return self.name


class District(models.Model):
    name = models.CharField(max_length=25)
    zone = models.ForeignKey(Zone)
    headquarter = models.CharField(max_length=254)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)

    def __str__(self):
        return self.name


def install_latlong():
    import json

    ds = '{"Achham":{"lat":29.05,"long":81.3},"Arghakhanchi":{"lat":28.0008333,"long":83.2466667},"Baglung":{"lat":28.2666667,"long":83.6},"Baitadi":{"lat":29.5185787,"long":80.4688061},"Bajhang":{"lat":29.55,"long":81.2},"Bajura":{"lat":29.4469444,"long":81.4866667},"Banke":{"lat":28.05,"long":81.6166667},"Bara":{"lat":27.0333333,"long":85},"Bardiya":{"lat":28.8166667,"long":80.4833333},"Bhaktapur":{"lat":27.673031,"long":85.427856},"Bhojpur":{"lat":27.1666667,"long":87.05},"Chitwan":{"lat":27.529131,"long":84.3542049},"Dadeldhura":{"lat":29.2992006,"long":80.5875862},"Dailekh":{"lat":28.8375,"long":81.7077778},"Dang-Deukhuri":{"lat":28,"long":82.2666667},"Darchula":{"lat":29.83,"long":80.55},"Dhading":{"lat":27.8666667,"long":84.9166667},"Dhankuta":{"lat":26.9833333,"long":87.3333333},"Dhanusa":{"lat":26.8350474,"long":86.0121573},"Dolakha":{"lat":27.7784288,"long":86.1751759},"Dolpa":{"lat":28.998686,"long":82.816437},"Doti":{"lat":29.266667,"long":80.983333},"Gorkha":{"lat":28,"long":84.6333333},"Gulmi":{"lat":28.088934,"long":83.2934086},"Humla":{"lat":29.9666667,"long":81.8333333},"Ilam":{"lat":26.9,"long":87.9333333},"Jajarkot":{"lat":28.73,"long":82.22},"Jhapa":{"lat":26.63982,"long":87.8942451},"Jumla":{"lat":29.2752778,"long":82.1833333},"Kailali":{"lat":28.6833333,"long":80.6},"Kalikot":{"lat":29.15,"long":81.6166667},"Kanchanpur":{"lat":28.2,"long":82.166667},"Kapilvastu":{"lat":27.5434407,"long":83.0549312},"Kaski":{"lat":28.28236,"long":83.866028},"Kathmandu":{"lat":27.702871,"long":85.318244},"Kavrepalanchok":{"lat":27.525942,"long":85.56121},"Khotang":{"lat":27.2317177,"long":86.8220341},"Lalitpur":{"lat":27.5419673,"long":85.3342973},"Lamjung":{"lat":28.2765491,"long":84.3542049},"Mahottari":{"lat":26.8761922,"long":85.80766},"Makwanpur":{"lat":27.3730012,"long":85.1894045},"Manang":{"lat":28.6666667,"long":84.0166667},"Morang":{"lat":26.6799002,"long":87.460397},"Mugu":{"lat":29.8666667,"long":82.6166667},"Mustang":{"lat":28.9985065,"long":83.8473015},"Myagdi":{"lat":28.611176,"long":83.5070203},"Nawalparasi":{"lat":27.6498409,"long":83.8897057},"Nuwakot":{"lat":27.97,"long":83.06},"Okhaldhunga":{"lat":27.3166667,"long":86.5},"Palpa":{"lat":27.8666667,"long":83.55},"Panchthar":{"lat":27.2036401,"long":87.8156715},"Parbat":{"lat":28.178049,"long":83.6986568},"Parsa":{"lat":26.8833333,"long":85.6333333},"Pyuthan":{"lat":28.1,"long":82.8666667},"Ramechhap":{"lat":27.3333333,"long":86.0833333},"Rasuwa":{"lat":27.083333,"long":86.433333},"Rautahat":{"lat":26.57,"long":86.53},"Rolpa":{"lat":28.3815621,"long":82.6483442},"Rukum":{"lat":28.7434423,"long":82.4752757},"Rupandehi":{"lat":27.6264239,"long":83.3789389},"Salyan":{"lat":28.28,"long":83.79},"Sankhuwasabha":{"lat":27.6141916,"long":87.1422895},"Saptari":{"lat":26.6172621,"long":86.7013894},"Sarlahi":{"lat":27.0084093,"long":85.520024},"Sindhuli":{"lat":27.2568824,"long":85.971322},"Sindhupalchok":{"lat":27.9512034,"long":85.684578},"Siraha":{"lat":26.656031,"long":86.208847},"Solukhumbu":{"lat":27.7909733,"long":86.6611083},"Sunsari":{"lat":26.6275522,"long":87.1821709},"Surkhet":{"lat":28.6,"long":81.6333333},"Syangja":{"lat":28.1046333,"long":83.8791074},"Tanahu":{"lat":27.944705,"long":84.2278796},"Taplejung":{"lat":27.35,"long":87.6666667},"Terhathum":{"lat":27.198391,"long":87.5000082},"Udayapur":{"lat":27.57,"long":82.9}}'
    ds = json.loads(ds)
    for dis in ds:
        try:
            district = District.objects.get(name=dis)
            district.latitude = ds[dis]['lat']
            district.longitude = ds[dis]['long']
            district.save()
        except District.DoesNotExist:
            print dis


class Company(models.Model):
    name = models.CharField(max_length=254)
    address = models.TextField(null=True, blank=True)
    district = models.ForeignKey(District)
    phone_no = models.CharField(max_length=30, null=True, blank=True)
    types = (('Company', 'Company'), ('DADO', 'DADO'))
    type = models.CharField(max_length=7, choices=types, default='Company')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = u'Companies'


def install_dados():
    import json

    ds = '{"Achham":{"lat":29.05,"long":81.3},"Arghakhanchi":{"lat":28.0008333,"long":83.2466667},"Baglung":{"lat":28.2666667,"long":83.6},"Baitadi":{"lat":29.5185787,"long":80.4688061},"Bajhang":{"lat":29.55,"long":81.2},"Bajura":{"lat":29.4469444,"long":81.4866667},"Banke":{"lat":28.05,"long":81.6166667},"Bara":{"lat":27.0333333,"long":85},"Bardiya":{"lat":28.8166667,"long":80.4833333},"Bhaktapur":{"lat":27.673031,"long":85.427856},"Bhojpur":{"lat":27.1666667,"long":87.05},"Chitwan":{"lat":27.529131,"long":84.3542049},"Dadeldhura":{"lat":29.2992006,"long":80.5875862},"Dailekh":{"lat":28.8375,"long":81.7077778},"Dang-Deukhuri":{"lat":28,"long":82.2666667},"Darchula":{"lat":29.83,"long":80.55},"Dhading":{"lat":27.8666667,"long":84.9166667},"Dhankuta":{"lat":26.9833333,"long":87.3333333},"Dhanusa":{"lat":26.8350474,"long":86.0121573},"Dolakha":{"lat":27.7784288,"long":86.1751759},"Dolpa":{"lat":28.998686,"long":82.816437},"Doti":{"lat":29.266667,"long":80.983333},"Gorkha":{"lat":28,"long":84.6333333},"Gulmi":{"lat":28.088934,"long":83.2934086},"Humla":{"lat":29.9666667,"long":81.8333333},"Ilam":{"lat":26.9,"long":87.9333333},"Jajarkot":{"lat":28.73,"long":82.22},"Jhapa":{"lat":26.63982,"long":87.8942451},"Jumla":{"lat":29.2752778,"long":82.1833333},"Kailali":{"lat":28.6833333,"long":80.6},"Kalikot":{"lat":29.15,"long":81.6166667},"Kanchanpur":{"lat":28.2,"long":82.166667},"Kapilvastu":{"lat":27.5434407,"long":83.0549312},"Kaski":{"lat":28.28236,"long":83.866028},"Kathmandu":{"lat":27.702871,"long":85.318244},"Kavrepalanchok":{"lat":27.525942,"long":85.56121},"Khotang":{"lat":27.2317177,"long":86.8220341},"Lalitpur":{"lat":27.5419673,"long":85.3342973},"Lamjung":{"lat":28.2765491,"long":84.3542049},"Mahottari":{"lat":26.8761922,"long":85.80766},"Makwanpur":{"lat":27.3730012,"long":85.1894045},"Manang":{"lat":28.6666667,"long":84.0166667},"Morang":{"lat":26.6799002,"long":87.460397},"Mugu":{"lat":29.8666667,"long":82.6166667},"Mustang":{"lat":28.9985065,"long":83.8473015},"Myagdi":{"lat":28.611176,"long":83.5070203},"Nawalparasi":{"lat":27.6498409,"long":83.8897057},"Nuwakot":{"lat":27.97,"long":83.06},"Okhaldhunga":{"lat":27.3166667,"long":86.5},"Palpa":{"lat":27.8666667,"long":83.55},"Panchthar":{"lat":27.2036401,"long":87.8156715},"Parbat":{"lat":28.178049,"long":83.6986568},"Parsa":{"lat":26.8833333,"long":85.6333333},"Pyuthan":{"lat":28.1,"long":82.8666667},"Ramechhap":{"lat":27.3333333,"long":86.0833333},"Rasuwa":{"lat":27.083333,"long":86.433333},"Rautahat":{"lat":26.57,"long":86.53},"Rolpa":{"lat":28.3815621,"long":82.6483442},"Rukum":{"lat":28.7434423,"long":82.4752757},"Rupandehi":{"lat":27.6264239,"long":83.3789389},"Salyan":{"lat":28.28,"long":83.79},"Sankhuwasabha":{"lat":27.6141916,"long":87.1422895},"Saptari":{"lat":26.6172621,"long":86.7013894},"Sarlahi":{"lat":27.0084093,"long":85.520024},"Sindhuli":{"lat":27.2568824,"long":85.971322},"Sindhupalchok":{"lat":27.9512034,"long":85.684578},"Siraha":{"lat":26.656031,"long":86.208847},"Solukhumbu":{"lat":27.7909733,"long":86.6611083},"Sunsari":{"lat":26.6275522,"long":87.1821709},"Surkhet":{"lat":28.6,"long":81.6333333},"Syangja":{"lat":28.1046333,"long":83.8791074},"Tanahu":{"lat":27.944705,"long":84.2278796},"Taplejung":{"lat":27.35,"long":87.6666667},"Terhathum":{"lat":27.198391,"long":87.5000082},"Udayapur":{"lat":27.57,"long":82.9}}'
    ds = json.loads(ds)
    for dis in ds:
        try:
            district = District.objects.get(name=dis)
            company = Company(name=dis, district=district, type='DADO')
            company.save()
        except District.DoesNotExist:
            print dis


class Source(models.Model):
    # from sheet.models import SourceData

    name = models.CharField(max_length=254)
    address = models.TextField(null=True, blank=True)
    district = models.ForeignKey(District)
    phone_no = models.CharField(max_length=30, null=True, blank=True)
    # varieties = models.ManyToManyField(Variety, through=SourceData)

    def __str__(self):
        return self.name