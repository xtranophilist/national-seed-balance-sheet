from django.conf.urls import patterns, url
from core import views


urlpatterns = patterns('',
                       url(r'^companies/(?P<region_id>[0-9]+).json$', views.companies_as_json, name='companies_as_json'),
                       url(r'^companies.json$', views.companies_as_json, name='companies_as_json'),
                       url(r'^sources/(?P<fiscal_year_id>[0-9]+)/(?P<crop_id>[0-9]+)/(?P<seed_class_id>[0-9]+).json$', views.sources_with_varieties_as_json, name='sources_with_varieties_as_json'),
                       url(r'^sources.json$', views.sources_as_json, name='sources_as_json'),
)
